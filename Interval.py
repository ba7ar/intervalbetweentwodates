from datetime import date
days = list(range(1, 32))
months = list(range(1, 13))
today = date.today()
print(today)


def inputInteger(message):
    while True:
        try:
            userInput = int(input(message))
        except ValueError:
            print("Please enter an integer.")
            continue
        else:
            return userInput
            break


def inputMonth(message):
    while True:
        message = inputInteger(message)
        if message in months:
            return message
            break
        else:
            print("Please pick a value from 1-12")


def inputDay(message):
    while True:
        message = inputInteger(message)
        if int(message) in days:
            return message
            break
        else:
            print("Please pick a value from 1-31")


year = int(inputInteger('Enter a year'))
month = int(inputMonth('Enter a month (1-12)'))
day = int(inputDay('Enter a day (1-31)'))
significantDate = date(year, month, day)
print("significant date:", significantDate, "of type", type(significantDate))
print("today:", today, "of type", type(today))

if today > significantDate:
    delta = today - significantDate
    print("Days since:", delta.days)
else:
    delta = significantDate - today
    print("Days left:", delta.days)
